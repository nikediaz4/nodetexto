'use strict'

var directorio = require('fs').readFileSync('locales/index.html'),
	http = require('http').createServer(webServer),
	querystring = require('querystring'),
	util = require('util'),
	dataPasar = ''


function webServer(require, response){

	if(require.method == 'GET')
	{
		response.writeHead(200, {"Content-Type": "text/html"})
		response.end(directorio)
	}

	if(require.method == 'POST')
	{
		require
			.on('data', function(data){
			dataPasar += data
		
		}).on('end', function(){
			var obtenerObjeto = querystring.parse(dataPasar),
				pasarJson = util.inspect(obtenerObjeto),

			mostrardata = `Se muestra el texto enviado: ${pasarJson}`
			console.log(mostrardata)
			response.end(mostrardata)

		})
	}

}

http.listen(8081)

console.log("Servidor Activo en: " + "http://localhost:8081")